import Link from 'next/link';
import DarkLayout from '../components/layouts/DarkLayout';
import MainLayout from '../components/layouts/MainLayout';

export default function AboutPage() {
  return (
    <>
      <h5 className='title'>Index</h5>
      <h5 className='title'>
        Welcome to <Link href='/pricing'>contact</Link>
      </h5>

      <p className='description'>
        Get started by editing <code className='code'>pages/about.js</code>
      </p>
    </>
  );
}

AboutPage.getLayout = function getLayout(page: JSX.Element) {
  return (
    <MainLayout>
      <DarkLayout>{page}</DarkLayout>
    </MainLayout>
  );
};
