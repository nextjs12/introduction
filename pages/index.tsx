import Link from 'next/link';
import MainLayout from '../components/layouts/MainLayout';

export default function HomePage() {
  return (
    <MainLayout>
      <h5 className='title'>Index</h5>
      <h5 className='title'>
        Welcome to <Link href='/about'>about</Link>
      </h5>

      <p className='description'>
        Get started by editing <code className='code'>pages/index.js</code>
      </p>
    </MainLayout>
  );
}
