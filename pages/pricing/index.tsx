import Link from 'next/link';
import React from 'react';
import MainLayout from '../../components/layouts/MainLayout';

const index = () => {
  return (
    <MainLayout>
      <h5 className='title'>Pricing</h5>
      <h5 className='title'>
        Welcome to <Link href='/'>index</Link>
      </h5>

      <p className='description'>
        Get started by editing <code className='code'>pages/about.js</code>
      </p>
    </MainLayout>
  );
};

export default index;
