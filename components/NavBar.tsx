import React from 'react';
import styles from './Navbar.module.css';
import ActiveLink from './ActiveLink';

const menuItems = [
  {
    text: 'Home',
    href: '/',
  },
  {
    text: 'About',
    href: '/about',
  },
  {
    text: 'Contact',
    href: '/contact',
  },
  {
    text: 'Pricing',
    href: '/pricing',
  },
];

const NavBar = () => {
  return (
    <nav className={styles['menu-container']}>
      {menuItems.map((item, key) => (
        <ActiveLink key={key} text={item.text} href={item.href} />
      ))}
    </nav>
  );
};

export { NavBar };
